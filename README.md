# Docker Example - Containers with shared volume

## The docker compose file

```
version: '3'
# Declare shared docker volume: data
volumes:
  data:

services:
  # Producer container write log messages to shared data volume
  producer:
    image: alpine
    command: sh -c 'while true ; do date >> /data/log; sleep 2; done'
    volumes:
      - data:/data

  # Consumer container read log messages from shared data volume
  consumer:
    image: alpine
    command: sh -c 'tail -f /data/log'
    volumes:
      - data:/data
    depends_on:
      - producer
```
## See it works

```
$ make up follow
consumer_1  | Sun Apr 30 05:44:00 UTC 2017
consumer_1  | Sun Apr 30 05:44:02 UTC 2017
consumer_1  | Sun Apr 30 05:44:04 UTC 2017
consumer_1  | Sun Apr 30 05:44:06 UTC 2017
consumer_1  | Sun Apr 30 05:44:08 UTC 2017
...
```
## Shut it down

```
$ make down
```





